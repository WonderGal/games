package hitlist;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Maitry Mehta Student id - 17964028
 */
public class HitList implements Encryptable 
{

    private ArrayList<String> targets;
    private boolean currentlyEncrypted;
    private char currentKey;

    /** In constructor
     * underlying List called targets is invoked
     * and boolean is set to false
     */
    public HitList() {
        this.targets = new ArrayList<>();
        this.currentlyEncrypted = false;
    }

    /**
     * the encrypt method is implemented from the Encryptable class 
     * the elements in the underlying target List is encrypted with the specific key and Caesar cipher algorithm
     * @param key 
     */
    @Override
    public void encrypt(char[] key)
    {
        String encryptedName = "";
        char encryptedVictim;
        this.currentKey = Character.toUpperCase(key[0]);
        if (!this.currentlyEncrypted) 
        {
            currentlyEncrypted = true;
            for (int i = 0; i < this.targets.size(); i++) 
            {
                for (int j = 0; j < this.targets.get(i).length(); j++) 
                {
                    char victimName = this.targets.get(i).charAt(j);
                    if (victimName >= 65 && victimName <= 90)
                    {
                        int victimNameMod = (victimName + this.currentKey) % 26;
                        encryptedVictim = (char) (victimNameMod + 65);
                        encryptedName += encryptedVictim;
                    }else {
                        
                    }
                }
                this.targets.set(i, encryptedName);
                encryptedName = "";
            }
            System.out.println(this.targets);
        }
       
    }

    /**
     * the encrypt method is implemented from the Encryptable class 
     * the elements in the underlying target List is decrypted with the specific key and Caesar cipher algorithm
     * Here all the elements rolls back into reverse direction by using the specific key.
     * @param key 
     */
    @Override
    public void decrypt(char[] key) 
    {
        String decryptedName = "";
        char decryptedVictim;
        this.currentKey = Character.toUpperCase(key[0]);
        if (this.currentlyEncrypted) 
        {
            currentlyEncrypted = false;
            for (int i = 0; i < this.targets.size(); i++) 
            {
                for (int j = 0; j < this.targets.get(i).length(); j++) 
                {
                    char victimName = this.targets.get(i).charAt(j);
                    if (victimName >= 65 && victimName <= 90)
                    {   
                        victimName +=78; 
                        int victimNameMod = (victimName - this.currentKey) % 26;
                        decryptedVictim = (char) (victimNameMod + 65);                      
                        decryptedName += decryptedVictim;
                    }else {
                        
                    }
                }
                this.targets.set(i, decryptedName);
                decryptedName = ""; 
            }
            System.out.println(this.targets);
        }
    }

    /**
     * 
     * @return 
     */
    @Override
    public boolean isEncrypted() 
    {
        return this.currentlyEncrypted;
    }

    /**
     * Here addVictim method adds the user input name of victim into uppercase
     * @param victim 
     */
    public void addVictim(String victim) 
    {
        this.targets.add(victim.toUpperCase());
    }

    /**
     * This method is used to remove the name of any person from the user input victim list 
     * if present or give appropriate error message
     * @param victim 
     */
    public void removeVictim(String victim) 
    {
        if (this.targets.contains(victim.toUpperCase()))
        {
            this.targets.remove(victim.toUpperCase());
        }
        else
        {
            System.out.println("This victim does not exist");
        }
        System.out.println(this.targets);
    }

    /**
     * 
     * @return 
     */
    @Override
    public String toString() 
    {
        return "";
    }

    /**
     * the main method has the Hitlist class invoked
     * It takes user input for creating victim list 
     * user input method to enter the key for encrypt and decrypt and
     * remove any persons name from victim list
     * 
     * 
     * @param args 
     */
    public static void main(String[] args)
    {
        HitList cryptography = new HitList();
        Scanner scan = new Scanner(System.in);
        String victimInput;
        String victimRemove;
        System.out.println("ENTER THE NAME OF VICTIMS or quit exit");
        do {
            victimInput = scan.nextLine();
            if (!victimInput.equalsIgnoreCase("QUIT") && !victimInput.isEmpty() && victimInput.matches("[a-zA-Z]+")) {
                cryptography.addVictim(victimInput);
            }
        } while (!victimInput.equalsIgnoreCase("QUIT"));
        System.out.println("ENTER THE KEY to encrypt ALL Items");
        char[] userKey = scan.next().toCharArray();
        System.out.println("HERE IS THE ENCRYPTED MESSAGE ");
        cryptography.encrypt(userKey);
        System.out.println("ENTER THE KEY to decrypt ALL Items");
        char[] userKeyToDecrypt = scan.next().toCharArray();
        cryptography.decrypt(userKeyToDecrypt);

        System.out.println("ENTER THE NAME OF VICTIMS to remove");
        victimRemove = scan.next();
        cryptography.removeVictim(victimRemove);
    }

}

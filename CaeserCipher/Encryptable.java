package hitlist;

/**
 * @author Maitry Mehta Student id - 17964028
 */
interface Encryptable {
    public void encrypt(char[] key);
    public void decrypt(char[] key);
    public boolean isEncrypted();   
}

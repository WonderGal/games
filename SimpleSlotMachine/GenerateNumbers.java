package simpleslotmachine;

import java.util.Random;

/**
 * @author Maitry Mehta Student id - 17964028
 */
public class GenerateNumbers
{
    public int number;
    public Random generator;
    
    /**
     *  
     */
    public GenerateNumbers() 
    {
        generator = new Random();
    }
    
    /**
     * This method calls fillSlot method which generates random number 
     * It has return type and so returns the value of the generated number
     * @return number
     */
    public int getNumber() 
    {
        this.fillSlot();
        return number;
    }

    /**
     * Random numbers between the range of 0-9 are generated in this method
     */
    private void fillSlot() 
    {
        number = generator.nextInt(9);
    }

}

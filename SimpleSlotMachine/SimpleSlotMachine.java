package simpleslotmachine;

import java.util.Scanner;

/**
 * @author Maitry Mehta Student id - 17964028
 */
public class SimpleSlotMachine 
{

    public int generateNumberOne;
    public int generateNumberTwo;
    public int generateNumberThree;
    public Scanner scan;
    public int spinCount;
    public GenerateNumbers generator;

    /**
     * In constructor the generator is invoked
     */
    public SimpleSlotMachine() 
    {
        generator = new GenerateNumbers();
    }

    /**
     * In this method three numbers are called from the getNumber method
     * Different conditions are checked for jackpot, two same number or different numbers and message is printed accordingly 
     */
    public void pullLever()
    {
        generateNumberOne = generator.getNumber();
        generateNumberTwo = generator.getNumber();
        generateNumberThree = generator.getNumber();
        
        if (generateNumberOne == generateNumberTwo && generateNumberTwo == generateNumberThree && generateNumberThree == generateNumberOne) {
            System.out.println("Your numbers are " + generateNumberOne + " " + generateNumberTwo + " " + generateNumberThree + "Jackpot");
        } else if (generateNumberOne == generateNumberTwo || generateNumberTwo == generateNumberThree || generateNumberThree == generateNumberOne) {
            System.out.println("Your numbers are " + generateNumberOne + " " + generateNumberTwo + " " + generateNumberThree + " Two Number same ");
        } else {
            System.out.println("Your numbers are " + generateNumberOne + " " + generateNumberTwo + " " + generateNumberThree);
        }
    }

    /**
     * Here the SimpleSlotMAchine is invoked
     * user input to play the game or not is taken
     * accordingly the lever is pulled and game starts
     * game is played until the user quits
     * counter is set to count the number of times the lever was pulled 
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {

        SimpleSlotMachine machine = new SimpleSlotMachine();
        Scanner scan = new Scanner(System.in);
        String userSelection;
        System.out.print("Enter Yes To Pull The Lever Or No To Quit :");
        userSelection = scan.nextLine();
        int gamesPlayed = 0;
        while ("yes".equalsIgnoreCase(userSelection))
        {
            machine.pullLever();
            gamesPlayed++;
            System.out.print("Play again (yes/no)?: ");
            userSelection = scan.nextLine();
        }
        System.out.print("PullLever called: " + gamesPlayed + " times");
    }
}

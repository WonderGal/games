package tennisgame;

/**
 * @author Maitry Mehta Student id - 17964028
 */
public class TennisGame {

    private int pointsServer;
    private int pointsReceiver;
    public final int SERVER = 1;
    public final int RECEIVER = 2;

    /**
     * 
     */
    public TennisGame() {
        this.pointsServer = 0;
        this.pointsReceiver = 0;
    }
    
    /**
     * This method converts pointsServer and pointReceiver to a string
     * which holds current scores for each player
     * @return score
     */
    public String getScore() {
        String serverPoints;
        String receiverPoints;
        if (pointsReceiver == 0) {
            receiverPoints = "love";
        } else {
            receiverPoints = pointsReceiver + "";
        }
        if (pointsServer == 0) {
            serverPoints = "love";
        } else {
            serverPoints = pointsServer + "";
        }

        String score = "Server " + serverPoints + ":" + "Receiver " + receiverPoints;

        if (pointsReceiver == 40 && pointsServer == 40) {
            score = "Duece";
        } else if (pointsReceiver == pointsServer && pointsReceiver < 40) {
            score = receiverPoints + " all";
        } else if (pointsReceiver == 1) {
            score = "Advantage receiver";
        } else if (pointsServer == 1) {
            score = "Advantage server";
        } else if (pointsReceiver > 40 || pointsServer > 40) {
            score = "game";
        }
        return score;
    }
    
    /**
     * This method gives a point using the conditional statement 
     * on constant SERVER and RECEIVER
     * 
     * @param player 
     */
    public void givePoint(int player) {
        if (player == SERVER) {
            if (pointsServer == 0) {
                pointsServer = 15;
            } else if (pointsServer == 15) {
                pointsServer = 30;
            } else if (pointsServer == 30) {
                pointsServer = 40;
            } else if (pointsServer == 40 && pointsReceiver == 40) {
                pointsServer = 1;
            } else if (pointsServer == 40 && pointsReceiver == 1) {
                pointsServer = 40;
                pointsReceiver = 40;
            } else if (pointsServer == 40 && pointsReceiver != 40) {
                pointsServer = 50; //Taking 50 as game point
            } else if (pointsServer == 1) {
                pointsServer = 50;
            }

        } else {
            if (pointsReceiver == 0) {
                pointsReceiver = 15;
            } else if (pointsReceiver == 15) {
                pointsReceiver = 30;
            } else if (pointsReceiver == 30) {
                pointsReceiver = 40;
            } else if (pointsReceiver == 40 && pointsServer == 40) {
                pointsReceiver = 1;
            } else if (pointsReceiver == 40 && pointsServer == 1) {
                pointsReceiver = 40;
                pointsServer = 40;
            } else if (pointsReceiver == 40 && pointsServer != 40) {
                pointsReceiver = 50;
            } else if (pointsReceiver == 1) {
                pointsReceiver = 50;
            }

        }

    }
    
    /**
     * The result of this method will be true if there is a winner 
     * @return result
     */
    public boolean isWinner() {
        boolean result = false;
        if (pointsReceiver > 40 || pointsServer > 40) {
            result = true;
        }
        return result;
    }
    
    /**
     * If either of the player are winner the result turns to be true
     * @param player
     * @return result
     */
    public boolean isWinner(int player) {
        boolean result = false;
        int points;
        if (player == SERVER) {
            points = pointsServer;
        } else {
            points = pointsReceiver;
        }
        if (points > 40) {
            result = true;
        }
        return result;
    }

}

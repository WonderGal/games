package tennisgame;

import java.util.Random;
import java.util.Scanner;

/**
 * @author Maitry Mehta Student id - 17964028
 */
public class TennisSet {

    private int scoreA;
    private int scoreB;
    private String playerA;
    private String playerB;
    private boolean play;
    
    /**
     * passes to players name into the constructor
     * @param playerA
     * @param playerB 
     */
    public TennisSet(String playerA, String playerB) {
        this.playerA = playerA;
        this.playerB = playerB;
    }
    
    /**
     * This method plays five games by passing in the loop playGame method
     */
    public void playSet() {
        for (int i = 0; i < 5; i++) {
            playGame();
        }
        System.out.println(playerA + " " + scoreA + ":" + playerB + " " + scoreB);
    }

    /**
     * Here the two players are randomly selected to be the winner 
     * Both the players are alternatively get the chance to serve for each game
     */
    public void playGame() {
        Random random = new Random();
        random.nextInt(2);
        TennisGame tennisGame = new TennisGame();
        if (play) {
            System.out.println("Server: " + playerB);
            System.out.println("Receiver: " + playerA);
        } else {
            System.out.println("Server: " + playerA);
            System.out.println("Receiver: " + playerB);
        }
        while (true) {
            System.out.println(tennisGame.getScore());
            int player = random.nextInt(2) + 1;
            tennisGame.givePoint(player);

            if (tennisGame.isWinner()) {
                if (tennisGame.isWinner(player)) {
                    String playerName;
                    if (!play) {
                        if (player == 1) {
                            playerName = this.playerA;
                            scoreA++;
                        } else {
                            playerName = this.playerB;
                            scoreB++;
                        }
                    } else {
                        if (player == 1) {
                            playerName = this.playerB;
                            scoreB++;
                        } else {
                            playerName = this.playerA;
                            scoreA++;
                        }
                    }

                    System.out.println(tennisGame.getScore());
                    System.out.println(playerName + " wins\n");
                    System.out.println("------------------------\n");

                    play = !play;
                    return;
                }
            }
        }

    }
    
    /**
     * The main method takes in the two players name and initializes the Tennis game
     * @param args 
     */
    public static void main(String[] args) {
        System.out.println("LETS PLAY A TENNIS GAME");
        System.out.println("Enter The Name Of Player A:");
        Scanner input = new Scanner(System.in);
        String PlayerA = input.nextLine();
        System.out.println("Enter The Name Of Player B:");
        String PlayerB = input.nextLine();
        TennisSet tennis = new TennisSet(PlayerA, PlayerB);
        tennis.playSet();
    }

}
